/**
const ipfsHttpClient = require('ipfs-http-client');
const ipfsCore = require('@mdip/ipfs-core');
const eccrypto = require('@toruslabs/eccrypto');
const ipns = require('ipns');
const { CID } = require('multiformats/cid');
const { toString: uint8ArrayToString } = require('uint8arrays/to-string');
const { fromString: uint8ArrayFromString } = require('uint8arrays/from-string');
const { base58btc } = require('multiformats/bases/base58');
const { base36 } = require('multiformats/bases/base36');
const Digest = require('multiformats/hashes/digest');
const last = require('it-last');
const WS = require('libp2p-websockets');
const transportKey = WS.prototype[Symbol.toStringTag];
const filters = require('libp2p-websockets/src/filters');

class Ipfs {
  constructor({ url, wsPort }) {
    this.url = url || 'http://localhost:5001';
    this.wsPort = wsPort || '4001';
    const urlFragment = this.url.split('://');
    const domain = urlFragment[1].split(':');
    this.domain = domain[0] || '127.0.0.1';
    if (this.domain === 'localhost') {
      this.domain = '127.0.0.1';
    }
    this.ipfsNodeAddrPrefix = `/ip4/${this.domain}/tcp/${this.wsPort}/ws/p2p/`;
    this.ipfsHttpClient = '';
    this.coreClient = '';
  }

  async connect() {
    this.coreClient = await ipfsCore.create({
      EXPERIMENTAL: { ipnsPubsub: true },
      libp2p: {
        config: {
          transport: {
            [transportKey]: {
              filter: filters.all
            }
          }
        }
      }
    });
    this.ipfsHttpClient = await ipfsHttpClient.create({ url: this.url });

    const { id: clientPeerId } = await this.ipfsHttpClient.id();
    const { id: corePeerId } = await this.coreClient.id();

    const ipfsNodeAddr = `${this.ipfsNodeAddrPrefix}${clientPeerId}`; // input it from the user.
    await this.coreClient.swarm.connect(ipfsNodeAddr);

    const peers = await this.coreClient.swarm.peers();
    const isPeerConnected = peers.find((peer) => ipfsNodeAddr.endsWith(peer.peer));
    if (!isPeerConnected) {
      throw new Error('Connection to IPFS Node failed');
    }
    const httpClientNamesysState = await this.ipfsHttpClient.name.pubsub.state();
    const coreClientNamesysState = await this.coreClient.name.pubsub.state();
    if (!httpClientNamesysState.enabled || !coreClientNamesysState.enabled) {
      throw new Error('IPNS Pubsub not enabled on both peers');
    }
    return { corePeerId, clientPeerId };
  }

  async disconnect() {
    await this.coreClient.stop();
    this.coreClient = '';
    this.ipfsHttpClient = '';
  }

  async sendJSONToIPFS(data) {
    if (!Array.isArray(data)) {
      throw new Error('Input must be an array');
    }
    const sanitizedData = data.map((obj, i) => {
      if (typeof obj !== 'object') {
        throw new Error(`Invalid input at index ${i}`);
      }
      return JSON.stringify(obj);
    });
    const storedCIDs = [];
    // eslint-disable-next-line no-restricted-syntax
    for await (const result of this.coreClient.addAll(sanitizedData)) {
      storedCIDs.push(result);
    }
    return storedCIDs;
  }

  async getData(cid) {
    let dData;
    // eslint-disable-next-line no-restricted-syntax
    for await (const chunk of this.coreClient.cat(cid)) {
      dData = chunk;
    }
    return dData.toString();
  }

  // deriveIPNSKeypair
  async deriveIPNSKeypair(name, privateKey) {
    const keypair = await this.coreClient.key.gen(name, {
      type: 'secp256k1',
      optPrivateKey: Buffer.from(privateKey, 'hex'),
    });
    last(
      this.ipfsHttpClient.name.resolve(keypair.id, {
        stream: false,
        timeout: 5e3, // 5sec
      }),
    ).catch(() => {});
    return keypair;
  }

  async linkToIPNS(ipfsAddr, keyName, keyId) {
    const namespace = '/record/';
    const b58 = base58btc.decode(`z${keyId}`);
    console.log('b58', b58);
    const ipnsKeys = ipns.getIdKeys(b58);
    console.log('ipnsKeys', ipnsKeys);
    const topic = `${namespace}${uint8ArrayToString(
      ipnsKeys.routingKey.uint8Array(),
      'base64url',
    )}`;
    console.log('topic', topic);

    await this.coreClient.pubsub.subscribe(topic, (msg) => {
      console.log('[coreClient]', msg);
    });
    await this.ipfsHttpClient.pubsub.subscribe(topic, (msg) => {
      console.log('[ipfsHttpClient]', msg);
    });

    const httpClientSubs = await this.ipfsHttpClient.pubsub.ls();
    console.log('[httpClientSubs]', httpClientSubs);
    if (
      !httpClientSubs ||
      !httpClientSubs.length ||
      !httpClientSubs.includes(topic)
    ) {
      console.warn('[httpClient could not subscribe]');
    }
    const coreClientSubs = await this.coreClient.pubsub.peers(topic);
    console.log('[coreClientSubs]', coreClientSubs);
    const { id: clientPeerId } = await this.ipfsHttpClient.id();
    if (
      !coreClientSubs ||
      !coreClientSubs.length ||
      !coreClientSubs.includes(clientPeerId)
    ) {
      console.warn('[coreClient could not find peer]');
    }

    const multihash = uint8ArrayFromString(keyId, 'base58btc');
    const digest = Digest.decode(multihash);
    const libp2pKey = CID.createV1(0x72, digest);
    const ipnsName = `/ipns/${libp2pKey.toString(base36)}`;

    const httpClientNamesysSubs = await this.ipfsHttpClient.name.pubsub.subs(); // API
    if (!httpClientNamesysSubs.includes(ipnsName)) {
      console.warn('IPNS path not found');
    }
    const resp = await this.coreClient.name.publish(ipfsAddr, {
      key: keyName,
      resolve: false,
      lifetime: '2400h',
    });
    return resp;
  }

  async resolveIPNS(ipnsPubKey) {
    let cid;
    // eslint-disable-next-line no-restricted-syntax
    for await (const name of this.ipfsHttpClient.name.resolve(ipnsPubKey)) {
      cid = name;
    }
    let dData;
    // eslint-disable-next-line no-restricted-syntax
    for await (const chunk of this.ipfsHttpClient.cat(cid)) {
      dData = chunk;
    }
    // return dData.toString();
    return uint8ArrayToString(dData);
  }

  // eslint-disable-next-line class-methods-use-this
  async encryptContent(data) {
    const encData = [];
    for (let i = 0; i < data.length; i += 1) {
      const { content, publicKey } = data[i];
      // eslint-disable-next-line no-await-in-loop
      const { ciphertext, ephemPublicKey, iv, mac } = await eccrypto.encrypt(
        Buffer.from(publicKey, 'hex'),
        Buffer.from(JSON.stringify(content)),
      );
      encData.push({
        encContent: {
          _c: ciphertext.toString('hex'),
          _e: ephemPublicKey.toString('hex'),
          _i: iv.toString('hex'),
          _m: mac.toString('hex'),
        },
        publicKey,
      });
    }
    return encData;
  }

  // eslint-disable-next-line class-methods-use-this
  async decryptContent(data, privateKeys) {
    const decData = [];
    for (let i = 0; i < data.length; i += 1) {
      const {
        encContent: { _c, _e, _i, _m },
      } = data[i];
      const privateKey = privateKeys[i];
      const payload = {
        ciphertext: Buffer.from(_c, 'hex'),
        ephemPublicKey: Buffer.from(_e, 'hex'),
        iv: Buffer.from(_i, 'hex'),
        mac: Buffer.from(_m, 'hex'),
      };
      // eslint-disable-next-line no-await-in-loop
      const plainText = await eccrypto.decrypt(
        Buffer.from(privateKey, 'hex'),
        payload,
      );
      const parsedPlainText = JSON.parse(plainText);
      decData.push(parsedPlainText);
    }
    return decData;
  }
}

module.exports = Ipfs;

 * 
 */
