const Web3 = require('web3');
// const Tx = require('ethereumjs-tx').Transaction;

const didUtils = exports;

/**
 * Function to sign an ETH tx.
 * @param {string} privKey
 * @param {string} rawTx
 * @param {string} pubKey
 * @returns {object}
 */
didUtils.signTx_UNSAFE = async (rawTx, pubKey, privKey) => {
  try {
    const web3 = new Web3(
      new Web3.providers.HttpProvider(rawTx.result.provider),
    );
    web3.eth.accounts.wallet.add(privKey);
    const tx = await web3.eth.accounts.signTransaction(
      {
        from: pubKey,
        gasPrice: rawTx.result.gasPrice,
        gas: rawTx.result.gasLimit,
        to: rawTx.result.contractAddress,
        data: rawTx.result.data,
      },
      privKey,
    );
    web3.eth.accounts.wallet.clear();
    return { success: true, data: tx };
  } catch (err) {
    return { success: false, data: `${err}` };
  }
};
