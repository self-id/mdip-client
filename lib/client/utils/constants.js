module.exports = {
  BTC_BLOCKCHAIN: 'btc',
  ETH_BLOCKCHAIN: 'eth',
  OMNI_BLOCKCHAIN: 'omni',
  IPFS: 'ipfs',
  TESTNET: 'testnet',

  W3C_DID_SCHEMA: 'https://www.w3.org/ns/did/v1',
  ALLOWED_CHAINS: ['btc', 'eth', 'omni', 'ipfs'],
};
