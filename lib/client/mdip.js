const bitcoin = require('bitcoinjs-lib');
const bitcoinMessage = require('bitcoinjs-message');
const Web3 = require('web3');

const web3 = new Web3();

const client = exports;

/**
 * Method to create a new verifiable presentation.
 * @param {string} blockchain
 * @param {string} vc
 * @param {string} publicKey
 * @param {string} privateKey
 * @param {string} challenge
 * @param {string} domain
 * @param {string} givenNetwork
 * @param {Object} randomBytes
 * @returns {Object}
 */
client.createVerifiablePresentation = async (
  blockchain,
  vc,
  publicKey,
  privateKey,
  challenge,
  domain,
  givenNetwork,
  randomBytes,
) => {
  const parsedVC = JSON.parse(vc);
  const vp = {
    '@context': ['https://www.w3.org/2018/credentials/v1'],
    type: 'VerifiablePresentation',
    verifiableCredential: [parsedVC],
  };
  const message = JSON.stringify(vp);
  let signature = null;
  if (blockchain === 'btc') {
    let network = bitcoin.networks.testnet;
    if (givenNetwork === 'mainnet') {
      network = bitcoin.networks.bitcoin;
    }
    const keyPair = bitcoin.ECPair.fromWIF(privateKey, network);
    const obatinedPrivKey = keyPair.privateKey;
    signature = bitcoinMessage.sign(
      message,
      obatinedPrivKey,
      keyPair.compressed,
      { extraEntropy: randomBytes(32) },
    );
  }
  if (blockchain === 'eth') {
    // const intmdSign = (await web3.eth.accounts.sign(message, `0x${privateKey}`)).signature;
    // signature = Buffer.from(intmdSign);
    signature = (await web3.eth.accounts.sign(message, `0x${privateKey}`)).signature;
  }
  if (blockchain === 'mongodb') {
    const network = bitcoin.networks.bitcoin;
    const keyPair = bitcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'), { network });
    signature = bitcoinMessage.sign(
      message,
      Buffer.from(privateKey, 'hex'),
      keyPair.compressed,
      { extraEntropy: randomBytes(32) },
    );
  }
  if (signature) {
    const createdAt = new Date().toISOString();
    vp.proof = {
      type: 'EcdsaSecp256k1VerificationKey2019',
      created: createdAt,
      proofPurpose: 'authentication',
      verificationMethod: publicKey, // TODO: use DID fragments to point to the public key.
      challenge,
      domain,
      jws: signature.toString('base64'),
    };
    return vp;
  }
  return null;
};

/**
 * Method to sign an ETH transaction.
 * @param {string} privKey
 * @param {string} rawTx
 * @param {string} pubKey
 * @param {{
  *  blockchain: string
  *  network: string
  * }} param0
  * @returns {Object}
  */
client.signTx = async (privKey, rawTx, pubKey, { blockchain, network } = {}) => {
  try {
    if (blockchain === 'btc' || blockchain === 'omni') {
      const nw = network === 'testnet'
        ? bitcoin.networks.testnet
        : bitcoin.networks.bitcoin;
      const psbtObj = bitcoin.Psbt.fromHex(rawTx);
      const signer = bitcoin.ECPair.fromWIF(privKey, nw);
      psbtObj.signAllInputs(signer);
      const valid = psbtObj.validateSignaturesOfAllInputs();
      if (valid) {
        psbtObj.finalizeAllInputs();
        const txHex = psbtObj.extractTransaction().toHex();
        return { success: true, data: txHex };
      }
    } else {
      const web3Instance = new Web3(
        new Web3.providers.HttpProvider(rawTx.result.provider),
      );
      web3Instance.eth.accounts.wallet.add(privKey);
      const tx = await web3Instance.eth.accounts.signTransaction(
        {
          from: pubKey,
          gasPrice: rawTx.result.gasPrice,
          gas: rawTx.result.gasLimit,
          to: rawTx.result.contractAddress,
          data: rawTx.result.data,
        },
        privKey,
      );
      web3Instance.eth.accounts.wallet.clear();
      return { success: true, data: tx };
    }
    return null;
  } catch (err) {
    return { success: false, data: `${err}` };
  }
};

/**
 * Method to decode a signed transaction
 * @param {string} signedTx signed transaction
 * @returns {Object} Transaction object
 */
client.decodeSignedTx = (signedTx, opts) => {
  try {
    const { blockchain, network } = opts;
    if (blockchain === 'omni' || blockchain === 'btc' ) {
      const nw = network === 'testnet'
        ? bitcoin.networks.testnet
        : bitcoin.networks.bitcoin;
      const tx = bitcoin.Transaction.fromHex(signedTx);
      tx.ins.forEach((input) => {
        // eslint-disable-next-line no-param-reassign
        input.txid = input.hash.reverse().toString('hex');
      });
      tx.outs.forEach((out) => {
        let address;
        let rawdata;
        try {
          if (bitcoin.script.toASM(out.script).startsWith('OP_RETURN')) {
            // eslint-disable-next-line prefer-destructuring
            rawdata = bitcoin.script.decompile(out.script)[1];
          } else {
            address = bitcoin.address.fromOutputScript(out.script, nw);
          }
        } catch (e) {
          throw new Error('Invalid output script provided');
        }
        if (address) {
          // eslint-disable-next-line no-param-reassign
          out.addresses = [address];
        }
        if (rawdata) {
          // eslint-disable-next-line no-param-reassign
          out.rawdata = rawdata.slice(9).toString(); // since starting 8 bytes are omnilayer data
        }
      });
      return tx;
    }
    throw new Error('Blockchain not supported');
  } catch (err) {
    throw new Error('Invalid transaction provided');
  }
};

/*
 * Method to create a raw transaction for sending payments.
 * @param {string} fromAddr sender address
 * @param {Object} toAddresses receiver address array
 * @param {Object} opts blockchain specific properties
 * @returns {string} rawTx The unsigned raw transaction.
 */
 client.preparePayment = async (fromAddr, toAddresses, opts) => {
  // TODO add bn.js for handling arithmetic operations
  try {
    const { blockchain, network, utxoData, rawdata } = opts;
    // BTC_BLOCKCHAIN means omnil layer btc
    if (blockchain === 'btc') {
      const { unspents, fee, nulldataFee } = utxoData;
      let totalAmount = 0;
      let finalFee = fee;
      let sendAmt = 0;
      if (rawdata) {
        finalFee = nulldataFee;
      }
      const totalSendAmt = toAddresses.reduce((acc, { amount }) => acc + amount, 0);
      let availableBalance = unspents.reduce((acc, { satoshis }) => acc + satoshis, 0);
      availableBalance = (availableBalance / 10 ** 8).toFixed(8);
      if (Number(availableBalance) < (totalSendAmt + Number(finalFee))) {
        throw new Error(`Insufficient balance. required amount: ${totalSendAmt + Number(finalFee)}, available amount: ${availableBalance}`);
      }
      const nw = network === 'testnet'
        ? bitcoin.networks.testnet
        : bitcoin.networks.bitcoin;
      const psbt = new bitcoin.Psbt({ network: nw });
      for (let i = 0; i < unspents.length; i++) {
        const {
          txid, outputIndex: vout, satoshis: amount, rawTx: { hex },
        } = unspents[i];
        totalAmount += amount;
        psbt.addInput({
          hash: txid,
          index: vout,
          nonWitnessUtxo: Buffer.from(hex, 'hex'),
        });
        if (Number((totalAmount / 10 ** 8).toFixed(8)) >= (totalSendAmt + Number(finalFee))) {
          break;
        }
      }
      for (let i = 0; i < toAddresses.length; i++) {
        const { address, amount } = toAddresses[i];
        psbt.addOutput({
          address,
          value: parseInt(amount * 1e8, 10),
        });
        sendAmt += amount;
      }
      if (rawdata) {
        const data = Buffer.from(rawdata).toString('hex');
        if (data.length > 144) {
          throw new Error('payment information can have at most 144 characters');
        }
        const omniSendAnydata = [
          '6f6d6e69', // omni
          '0000', // version
          '00',
          'c8',
          `${data}`, // raw data 72 bytes max
        ].join('');
        const embed = bitcoin.payments.embed({ data: [Buffer.from(omniSendAnydata, 'hex')] });
        psbt.addOutput({
          script: embed.output,
          value: 0,
        });
      }
      const change = parseInt(
        totalAmount - (sendAmt + Number(finalFee)) * 1e8,
        10,
      );
      if (change > 0) {
        psbt.addOutput({
          address: fromAddr,
          value: change,
        });
      }
      return psbt.toHex();
    }
    return null;
  } catch (error) {
    throw new Error(error);
  }
};
