/**
 * Expose all the modules of the MDIProtocol
 *
 * @exports mdip
 * @type {Object}
 */
const wallet = require('./wallet/hd');
const btcDID = require('./client/utils/btc-did');
const ethDID = require('./client/utils/eth-did');
// const IPFS = require('./client/utils/ipfs');

module.exports = {
  wallet,
  btcDID,
  ethDID,
  // IPFS,
};
